FROM node:14.1-alpine AS api-builder

WORKDIR /usr/src/partydrink-api
COPY package.json package-lock.json ./
RUN npm install

COPY . ./

ARG ENV_FILE
RUN echo -e $ENV_FILE >> .env

ENV DB default_unset_value

RUN cat ./.env

RUN npm run compile

CMD cat ./.env && printenv && node ./dist/index.js

