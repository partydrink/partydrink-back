import { NullableId, Id } from "@feathersjs/feathers";
import app from "../app";
import { QuestionType } from "./questionType.class";
import { User } from "./user.class";

export interface QuestionModel {
  _id: NullableId;
  typeId: NullableId;
  text: string;
  difficulty: number;
  hotLevel: number;
  creatorId: NullableId;
  creationDate: Date;
  updaterId: NullableId;
  updateDate: Date;
  displayDrinks: boolean;
}

enum QuestionErrors {
  NotFound = "Question Not Found",
  text = "Invalid Text",
  difficulty = "Invalid Difficulty",
  hotLevel = "Invalid HotLevel",
  type = "Invalid Type",
  typeId = "Invalid TypeId",
  displayDrinks = "Invalid DisplayDrinks",
}

export class Question {
  static readonly Errors = QuestionErrors;

  private _id: NullableId = null;
  public get id(): NullableId {
    return this._id;
  }
  public set id(value: NullableId) {
    this._id = value;
  }

  private _type: QuestionType = new QuestionType();
  public get type(): QuestionType {
    return this._type;
  }
  public set type(value: QuestionType) {
    this._type = value;
  }

  private _text: string = "";
  public get text(): string {
    return this._text;
  }
  public set text(value: string) {
    this._text = value;
  }

  private _difficulty: number = 0;
  public get difficulty(): number {
    return this._difficulty;
  }
  public set difficulty(value: number) {
    this._difficulty = value;
  }

  private _hotLevel: number = 0;
  public get hotLevel(): number {
    return this._hotLevel;
  }
  public set hotLevel(value: number) {
    this._hotLevel = value;
  }

  private _creator: User = new User();
  public get creator(): User {
    return this._creator;
  }
  public set creator(value: User) {
    this._creator = value;
  }

  private _creationDate: Date = new Date();
  public get creationDate(): Date {
    return this._creationDate;
  }
  public set creationDate(value: Date) {
    this._creationDate = value;
  }

  private _updater: User = new User();
  public get updater(): User {
    return this._updater;
  }
  public set updater(value: User) {
    this._updater = value;
  }

  private _updateDate: Date = new Date();
  public get updateDate(): Date {
    return this._updateDate;
  }
  public set updateDate(value: Date) {
    this._updateDate = value;
  }

  private _displayDrinks: boolean = false;
  public get displayDrinks(): boolean {
    return this._displayDrinks;
  }
  public set displayDrinks(value: boolean) {
    this._displayDrinks = value;
  }

  /**
   *
   */
  constructor() {}

  public get nbPlayers(): number {
    const playerParameters = this.text.match(
      Question.getPlayerParametersRegex()
    );

    if (playerParameters) {
      return playerParameters.length + (this.type.isGroup ? 0 : 1);
    } else {
      return this.type.isGroup ? 0 : 1;
    }
  }

  public static getPlayerParametersRegex() {
    return Question.getParametersRegex(
      textParametersList.filter((p) => p.playerParameter)
    );
  }

  public static getAllParametersRegex() {
    return Question.getParametersRegex(textParametersList);
  }

  private static getParametersRegex(parameterList: TextParameter[]) {
    const reg = parameterList
      .reduce((regexString, textParameter) => {
        if (textParameter.options) {
          const optionRegexStr = Object.entries(textParameter.options)
            .reduce((r, option) => {
              if (typeof option[1] === "number") {
                return `${r}${option[0]}:[0-9]+;`;
              } else if (typeof option[1] === "string") {
                return `${r}${option[0]}:[a-zA-Z]+;`;
              } else {
                return "";
              }
            }, "")
            .slice(0, -1);
          return `${regexString}{${textParameter.value}\\(${optionRegexStr}\\)}|`;
        } else {
          return `${regexString}{${textParameter.value}}|`;
        }
      }, "")
      .slice(0, -1);

    return new RegExp(reg, "g");
  }

  public static New(datas: Partial<Question>): Question {
    return Object.assign(new Question(), datas);
  }

  public static async fromDbToClass(datas: any): Promise<Question> {
    let r = new Question();

    r.id = datas._id;
    try {
      r.type = await app.services["question-types"].get(datas.typeId as Id);
    } catch (error) {
      if (error.code === 404) r.type = new QuestionType();
      else throw error;
    }
    r.text = datas.text;
    r.difficulty = datas.difficulty;
    r.hotLevel = datas.hotLevel;
    if (datas.creatorId) {
      try {
        r.creator = await app.services["users"].get(datas.creatorId as Id);
      } catch (error) {
        if (error.code === 404) r.creator = new User();
        else throw error;
      }
    } else {
      r.creator = new User();
    }
    r.creationDate = datas.creationDate;

    if (datas.updaterId) {
      try {
        r.updater = await app.services["users"].get(datas.updaterId as Id);
      } catch (error) {
        if (error.code === 404) r.updater = new User();
        else throw error;
      }
    } else {
      r.updater = new User();
    }
    r.updateDate = datas.updateDate;
    r.displayDrinks = datas.displayDrinks;

    return r;
  }

  public static fromFrontToDb(datas: any): Partial<QuestionModel> {
    let dbDatas: Partial<QuestionModel> = {
      difficulty: datas.difficulty,
      hotLevel: datas.hotLevel,
      text: datas.text,
      displayDrinks: datas.displayDrinks,
    };

    if (datas.type) dbDatas.typeId = datas.type.id;
    else if (datas.typeId) dbDatas.typeId = datas.typeId;

    return dbDatas;
  }
}

export interface TextParameter {
  text: string;
  value: string;
  playerParameter?: boolean;
  options?: TextParameterOptions;
}

export interface TextParameterOptions {
  min?: number;
  max?: number;
}

export const textParametersList: TextParameter[] = [
  {
    text: "Random player",
    value: "player",
    playerParameter: true,
  },
  {
    text: "Random girl player",
    value: "playerGirl",
    playerParameter: true,
  },
  {
    text: "Random man player",
    value: "playerMan",
    playerParameter: true,
  },
  {
    text: "Random number",
    value: "number",
    options: {
      min: 0,
      max: 5,
    },
  },
];

const reg = textParametersList
  .reduce((regexString, textParameter) => {
    if (textParameter.options) {
      const optionRegexStr = Object.entries(textParameter.options)
        .reduce((r, option) => {
          if (typeof option[1] === "number") {
            return `${r}${option[0]}:[0-9]+;`;
          } else if (typeof option[1] === "string") {
            return `${r}${option[0]}:[a-zA-Z]+;`;
          } else {
            return "";
          }
        }, "")
        .slice(0, -1);
      return `${regexString}{${textParameter.value}\\(${optionRegexStr}\\)}|`;
    } else {
      return `${regexString}{${textParameter.value}}|`;
    }
  }, "")
  .slice(0, -1);

// console.log(reg);

// console.log("{player}".replace(/[\{]/g, "'"));

export const textParametersRegex: RegExp = new RegExp(reg, "g");
